<?php

class BlogController extends ControllerBase {

	public function categoryListAction() {
		$categories = R::find("blog_categories");

		$this->view->output("blog/category_list.html", ['categories' => $categories]);
	}

	public function categoryEditAction() {
		if($this->request->method('post')) {
			$category_data = $this->request->param('category');

			if($category_data['id'] == 0) {
				$category = R::dispense('blog_categories');

				$category_data['slug'] = $this->slugify($category_data['name']);

				$category->name = $category_data['name'];
				$category->slug = $category_data['slug'];

				R::store($category);

			} else {
				$category = R::load('blog_categories', $category_data['id']);

				$category->name = $category_data['name'];
				$category->slug = $category_data['slug'];

				R::store($category);
			}

			$this->response->redirect("/admin/blog/category/list")->send();
			return;
		}

		$category = [
			'name' => ''
		];

		if($this->request->param('id') != 0) {
			$category = R::load("blog_categories", $this->request->param('id'));
		}

		$this->view->output("blog/category_edit.html", ['category' => $category]);
	}

	public function articleListAction() {
		$articles = R::find("blog_articles");

		$this->view->output("blog/article_list.html", ['articles' => $articles]);
	}

	public function articleEditAction() {
		if($this->request->method('post')) {
			$article_data = $this->request->param('article');

			if($article_data['id'] == 0) {
				$article = R::dispense('blog_articles');

				$article_data['slug'] = $this->slugify($article_data['title']);

				$article->title = $article_data['title'];
				$article->slug = $article_data['slug'];
				$article->content = $article_data['content'];
				$article->category_id = $article_data['category_id'];

				R::store($article);

			} else {
				$article = R::load('blog_articles', $article_data['id']);

				$article->title = $article_data['title'];
				$article->slug = $article_data['slug'];
				$article->content = $article_data['content'];
				$article->category_id = $article_data['category_id'];

				R::store($article);
			}

			$this->response->redirect("/admin/blog/article/list")->send();
			return;
		}

		$article = [
			'title' => '',
			'content' => '',
			'category_id' => 0
		];

		if($this->request->param('id') != 0) {
			$article = R::load("blog_articles", $this->request->param('id'));
		}

		$categories = R::find('blog_categories');

		$this->view->output("blog/article_edit.html", ['article' => $article, 'categories' => $categories]);
	}
}