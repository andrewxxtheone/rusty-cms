<?php

class BlogProvider implements IDataProvider {

	public function getPosts() {
		return R::find("blog_articles");
	}

	public function getPost($slug) {
		return R::findOne("blog_articles", "slug = ?", [$slug]);
	}
}