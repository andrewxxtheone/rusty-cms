<?php

class BlogController extends ControllerBase {

	public function viewAction() {

		$bp = new BlogProvider();

		$this->view->output("pages/blog-article.html", ['article' => $bp->getPost($this->request->param('slug'))]);
	}
}