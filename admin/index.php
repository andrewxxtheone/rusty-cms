<?php

require_once "AutoLoader.php";

$di = new DI();

$klein = new \Klein\Klein();

$di->set("request", function() use(&$klein) {
	return $klein->request();
});

$di->set("response", function() use(&$klein) {
	return $klein->response();
});

$di->set("view", function() {
	$core = new \Dwoo\Core();
	$core->setCompileDir('admin/cache/views/'); 
	$core->setTemplateDir('admin/views/');
	return $core;
});

$di->set("session", function() {
	$sess = new SessionWrapper();
	return $sess;
});

$di->set("auth", function() use(&$di) {
	$auth = new AuthManager($di);
	return $auth;
});

$url_whitelist = [
	'/admin/signin'
];

$klein->respond(function ($request, $response, $service, $app) use($di, $url_whitelist) {
	$service->DI = $di;

	if(!$di->auth->isAuthed()) {
		if(!in_array($_SERVER['REQUEST_URI'], $url_whitelist)) {
			$response->redirect('/admin/signin', 302)->send();
			return;
		}
	}
});

$klein->with("/admin", function() use (&$klein) {
	$klein->respond('/', ["IndexController", "indexAction"]);
	$klein->respond('/signin', ["AuthController", "signinAction"]);
	$klein->respond('/signout', ["AuthController", "signoutAction"]);

	$klein->with('/pages', function() use(&$klein) {
		$klein->respond('/edit', ["PageController", "editAction"]);
		$klein->respond('/list', ["PageController", "listAction"]);
	});






/*PLUGIN ROUTES*/






	$klein->with('/plugins', function() use(&$klein) {
		$klein->respond('/install', ["PluginController", "installAction"]);
	});

	$klein->respond('/[a:controller]/[a:action]/[:param1]?/[:param2]?/[:param3]?/[:param4]?', function($request, $response, $service, $app) use(&$klein) {
	    $refClass = new \ReflectionClass(ucwords($request->param('controller'))."Controller");
	    $instance = $refClass->newInstance();

	    $instance->setKlein($klein);

	    call_user_func_array([$instance, $request->param('action')."Action"], [
	    	$request->param('param1'),
	    	$request->param('param2'),
	    	$request->param('param3'),
	    	$request->param('param4')
	    ]);
	});
});

$klein->dispatch();