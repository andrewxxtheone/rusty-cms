<?php
class AuthController extends ControllerBase {


	public function signinAction() {

		if($this->request->method('post')) {
			

			if($this->auth->login($this->request->param('email'), $this->request->param('password'))) {
				$this->response->redirect('/admin')->send();
				return;
			}

		}

		$this->view->output("signin.html");
	}

	public function signoutAction() {

		session_destroy();

		$this->response->redirect('/admin')->send();
	}
}