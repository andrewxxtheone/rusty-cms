<?php

class PageController extends ControllerBase {
	public function delTree($dir) { 
	   $files = array_diff(scandir($dir), array('.','..')); 
	    foreach ($files as $file) { 
	      (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file"); 
	    } 
	    return rmdir($dir); 
	  }


	public function listAction() {
		$pages = R::find("page");

		$this->view->output("pages/list.html", ['pages' => $pages]);
	}

	public function editAction() {

		if($this->request->method('post')) {
			$page_data = $this->request->param('page');

			if($page_data['id'] == 0) {
				$page = R::dispense('page');

				$page_data['slug'] = $this->slugify($page_data['title']);

				$page->title = $page_data['title'];
				$page->slug = $page_data['slug'];
				$page->template = $page_data['template'];
				$page->type = $page_data['type'];
				$page->layout_id = $page_data['layout_id'];
				$page->data_provider = $page_data['data_provider'];
				$page->content_tag = $page_data['content_tag'];

				R::store($page);

			} else {
				$page = R::load('page', $page_data['id']);

				$page->title = $page_data['title'];
				$page->template = $page_data['template'];
				$page->type = $page_data['type'];
				$page->layout_id = $page_data['layout_id'];
				$page->data_provider = $page_data['data_provider'];
				$page->content_tag = $page_data['content_tag'];

				R::store($page);
			}

			$file_path = "app/views/".$page->type."s";

			$content = "";

			if($page->layout_id != 0) {

				$layout = R::findOne("page", "id = ?", [$page->layout_id]);

				$content .= "{extends \"/layouts/".$layout->slug.".html\"}\n";
				$content .= "{block \"".$page->content_tag."\"}\n";

			}
			
			$content .= $page->template;

			if($page->layout_id != 0) {
				$content .= "\n{/block}";
			}

			@mkdir($file_path, 0777, true);
			$this->delTree("app/cache/views/app");
			file_put_contents($file_path."/".$page->slug.".html", $content);
			
			$this->response->redirect("/admin/pages/list")->send();
			return;
		}

		$page = [
			'title' => '',
			'template' => '',
			'type' => 'page',
			'data_provider' => ''
		];

		if($this->request->param('id') != 0) {
			$page = R::load("page", $this->request->param('id'))->export();
		}

		$layouts = R::find('page', 'type = ?', ['layout']);

		$html_editor = true;

		if( in_array($page['type'], ['layout', 'partial']) || ($this->request->param('html_editor') == 'no')  ) {
			$html_editor = false;
		}

		$this->view->output("pages/edit.html", ['html_editor' => $html_editor, 'page' => $page, 'layouts' => $layouts]);
	}
}