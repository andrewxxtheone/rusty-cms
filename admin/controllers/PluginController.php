<?php

class PluginController extends ControllerBase {

	public function recurse_copy($src,$dst) { 
	    $dir = opendir($src); 
	    @mkdir($dst); 
	    while(false !== ( $file = readdir($dir)) ) { 
	        if (( $file != '.' ) && ( $file != '..' )) { 
	            if ( is_dir($src . '/' . $file) ) { 
	                $this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
	            } 
	            else { 
	                copy($src . '/' . $file,$dst . '/' . $file); 
	            } 
	        } 
	    } 
	    closedir($dir); 
	} 

	public function delTree($dir) { 
	   $files = array_diff(scandir($dir), array('.','..')); 
	    foreach ($files as $file) { 
	      (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file"); 
	    } 
	    return rmdir($dir); 
	  }

	public function installAction() {

		if($this->request->method('post')) {
			move_uploaded_file($_FILES["file"]["tmp_name"], "admin/cache/upload/plugin.zip");

			$plugin_name = $this->extractPlugin();
			$this->installFiles($plugin_name);
			$this->installFrontendViews($plugin_name);
			$this->setAdminRoutes($plugin_name);
			$this->setFrontendRoutes($plugin_name);
			$this->setAdminMenuItems($plugin_name);

			$this->delTree("admin/cache/views/admin");
			$this->delTree("admin/cache/upload");
			@mkdir("admin/cache/upload");
			
			die("ok");
		}

		$plugins = R::find("plugin");

		$this->view->output("plugins/installer.html", ['plugins' => $plugins]);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function uninstallAction($plugin_name)
	{
		$pluginBean = R::findOne("plugin", "slug = ?", [$plugin_name]);


		$l_re = "/(\\{\\* PLUGIN \\<".$pluginBean->slug."\\> START \\*\\}[\\s\\S]*\\{\\* PLUGIN \\<".$pluginBean->slug."\\> END \\*\\})/"; 
		$a_re = "/(\\/\\*PLUGIN \\<".$pluginBean->slug."\\> START\\*\\/[\\s\\S]*\\/\\*PLUGIN \\<".$pluginBean->slug."\\> END\\*\\/)/"; 
	
		$lay_content = file_get_contents("admin/views/layout.html");
		file_put_contents("admin/views/layout.html", preg_replace($l_re, "", $lay_content));

		$a_index = file_get_contents("admin/index.php");
		file_put_contents("admin/index.php", preg_replace($a_re, "", $a_index));

		$a_index = file_get_contents("app/index.php");
		file_put_contents("app/index.php", preg_replace($a_re, "", $a_index));

		if(isset($pluginBean->templates)) {

			$templates = explode(",", $pluginBean->templates);

			foreach($templates as $t) {
				$pageBean = R::findOne("page", "slug = ?", [$t]);
				R::trash($pageBean);
			}
		}

		R::trash($pluginBean);
		
		$this->delTree("admin/cache/views/admin");

		die("Success..");
	}

	/*utilities*/

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function extractPlugin()
	{
		$zip = new ZipArchive();
		$zip->open('admin/cache/upload/plugin.zip', ZipArchive::CREATE);
		$zip->extractTo('admin/cache/upload');

		$dir = trim($zip->getNameIndex(0), '/');
		$zip->close();

		$info = $this->getPluginInfo($dir);

		sleep(1);
		rename("admin/cache/upload/".$dir, "admin/cache/upload/".$this->slugify($info['name']));

		$pluginBean = R::dispense("plugin");
		$pluginBean->name = $info['name'];
		$pluginBean->slug = $this->slugify($info['name']);
		$pluginBean->description = (array_key_exists('description', $info)?$info['description']:'');

		R::store($pluginBean);

		return $this->slugify($info['name']);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function installFiles($plugin_name)
	{
		$this->recurse_copy("admin/cache/upload/".$plugin_name."/admin", "admin");
		$this->recurse_copy("admin/cache/upload/".$plugin_name."/app", "app");
		$this->recurse_copy("admin/cache/upload/".$plugin_name."/content", "content");
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function installFrontendViews($plugin_name)
	{
		$re = "/\\{extends \\\"[\\s\\S]+\\\"\\}\\r\\n\\{block \\\"content\\\"\\}([\\t\\n\\r\\S\\s]*)\\{\\/block\\}([\\t\\n\\r\\S\\s]*)\\z/miUu"; 
		$path = "admin/cache/upload/".$plugin_name."/views";
		$slugs = [];

		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				if ('.' === $file) continue;
				if ('..' === $file) continue;
				//echo $file."<br>";
				$file_name = $file;
				$file_path = $path ."/".$file_name;
				$template_name = explode(".", $file_name);
				$template_name = str_replace("-", "_", $template_name[0]);
				$template_name = $this->underscore2Camelcase($template_name);

				$content = file_get_contents($file_path);

				//var_dump($content);

				//$content = "{extends \"/layouts/layout-1-column-1.html\"}\n{block \"content\"}\n    <div class=\"panel panel-default\">\n        <div class=\"panel-heading clearfix\"><span class=\"pull-right\"></span>\n<div class=\"pull-left\"><h4 style=\"margin-top: 15px; margin-left: 10px\">{\$product.title}</h4></div>\n</div>\n<div class=\"panel-body light-blue row\">\n<div class=\"col-lg-6\">\n\n<div id=\"carousel-example-generic\" class=\"carousel slide\" data-ride=\"carousel\">\n  <!-- Indicators -->\n  <ol class=\"carousel-indicators\">\n{\$itr = 0}\n{foreach \$product.images val}\n    <li data-target=\"#carousel-example-generic\" data-slide-to=\"{\$itr}\" class=\"{tif \$itr == 0 ? \"active\"}\"></li>\n{\$itr = \$itr+1}\n{/foreach}\n  </ol>\n\n  <!-- Wrapper for slides -->\n  <div class=\"carousel-inner\" role=\"listbox\">\n{\$itr = 0}\n{foreach \$product.images val}\n    <div class=\"item {tif \$itr == 0 ? \"active\"}\">\n      <img src=\"{\$val}\">\n    </div>\n{\$itr = \$itr+1}\n{/foreach}\n  </div>\n\n  <!-- Controls -->\n  <a class=\"left carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"prev\">\n    <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>\n    <span class=\"sr-only\">Előző</span>\n  </a>\n  <a class=\"right carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"next\">\n    <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>\n    <span class=\"sr-only\">Következő</span>\n  </a>\n</div>\n\n</div>\n\n<div class=\"col-lg-6\">\n<div class=\"row\">\n<div class=\"col-lg-6\"><del>{\$product.price} Ft</del></div>\n<div class=\"col-lg-6\"><b style=\"font-size: 22pt;color: red;\">{\$product.discount_price} Ft</b></div>\n</div>\n\n<div class=\"row\">\n<div class=\"col-lg-4\">Kategóriák</div>\n<div class=\"col-lg-8\">\n{foreach \$product.categories val implode=\", \"}\n<a href=\"/product/search/{\$val.slug}\">{\$val.name}</a>\n{/foreach}\n</div>\n</div>\n\n\n<div class=\"row\">\n<div class=\"col-lg-4\">Cimkék</div>\n<div class=\"col-lg-8\">\n{foreach \$product.tags val implode=\", \"}\n<a href=\"/product/search?query={\$val}\">{\$val}</a>\n{/foreach}\n</div>\n</div>\n\n<div class=\"row\">\n<div class=\"col-lg-12\">\n<form action=\"/cart/add\" method=\"post\">\n<input type=\"hidden\" name=\"product_data[product_id]\" value=\"{\$product.id}\">\n<input type=\"hidden\" name=\"product_data[slug]\" value=\"{\$product.slug}\">\n<input type=\"hidden\" name=\"product_data[title]\" value=\"{\$product.title}\">\n<input type=\"hidden\" name=\"product_data[coverimage_src]\" value=\"{\$product.images[0]}\">\n<input type=\"hidden\" name=\"product_data[price]\" value=\"{\$product.discount_price}\">\n<input type=\"hidden\" name=\"product_data[quantity]\" value=\"1\">\n<button type=\"submit\" class=\"button btn btn-primary button-primary primary\">\n<i class=\"fa fa-shopping-cart fa-cart\"></i> Kosárba!\n</button>\n</form>\n</div>\n</div>\n</div>\n</div>\n        <div class=\"panel-body\">\n{\$product.description}\n        </div>\n    </div>\n\n{/block}\n{block og_meta}\n{\$itr = 0}\n{foreach \$product.images val}\n<meta property=\"og:image\" content=\"{\$val}\">\n{\$itr = \$itr+1}\n{/foreach}\n            <meta property=\"og:url\" content=\"http://vip.bonzoportal.hu/product/view/{\$product.slug}\">\n    <meta property=\"og:title\" content=\"Bonzoportál - {\$product.title}\">\n    <meta property=\"og:description\" content=\"{wordwrap stripTags(\$product.description) 40}\">\n{/block}";
				preg_match($re, $content, $matches);
				$template = $matches[1];
				$others = "";
				if(array_key_exists(2, $matches)) {
					$others = $matches[2];
				}

				$page = R::dispense('page');
				$page->title = $template_name;
				$page->template = $template;
				$slugs[] = $page->slug = $this->slugify($template_name);
				$page->type = "page";
				$page->layout_id = 0;
				$page->data_provider = "";
				$page->content_tag = "content";
				$page->other_blocks = $others;

				R::store($page);
			}
			closedir($handle);
		}

		$pluginBean = R::findOne("plugin", "slug = ?", [$plugin_name]);
		$pluginBean->templates = implode(",", $slugs);
		R::store($pluginBean);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function setAdminRoutes($plugin_name)
	{
		$info = $this->getPluginInfo($plugin_name);

		$admin_routes = [];
		foreach($info['admin_routes'] as $admin_route) {

			$line = '$klein->respond(\''.$admin_route['route'].'\', ["'.$admin_route['controller'].'", "'.$admin_route['method'].'"]);';

			$admin_routes[] = $line;
		}

		$admin_index = file_get_contents("admin/index.php");
		$lines = '/*PLUGIN <'.$this->slugify($info['name']).'> START*/'."\n";
		$lines .= implode("\r\n", $admin_routes)."\n";
		$lines .= '/*PLUGIN <'.$this->slugify($info['name']).'> END*/'."\n\n";
		$lines .= '/*PLUGIN ROUTES*/'."\n\n";

		$admin_index = str_replace('/*PLUGIN ROUTES*/', $lines, $admin_index);
		file_put_contents("admin/index.php", $admin_index);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function setFrontendRoutes($plugin_name)
	{

		$info = $this->getPluginInfo($plugin_name);

		$app_routes = [];
		foreach($info['app_routes'] as $app_route) {

			$line = '$klein->respond(\''.$app_route['route'].'\', ["'.$app_route['controller'].'", "'.$app_route['method'].'"]);';

			$app_routes[] = $line;
		}

		$app_index = file_get_contents("app/index.php");
		$lines = '/*PLUGIN <'.$this->slugify($info['name']).'> START*/'."\n";
		$lines .= implode("\r\n", $app_routes)."\n";
		$lines .= '/*PLUGIN <'.$this->slugify($info['name']).'> END*/'."\n\n";
		$lines .= '/*PLUGIN ROUTES*/'."\n\n";

		$app_index = str_replace('/*PLUGIN ROUTES*/', $lines, $app_index);
		file_put_contents("app/index.php", $app_index);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function setAdminMenuItems($plugin_name)
	{
		$info = $this->getPluginInfo($plugin_name);

		$items = [];
		foreach($info['admin_menu'] as $menu) {
			$items[] = '<li><a href="'.$menu['url'].'">'.$menu['name'].'</a></li>';
		}

		$admin_layout = file_get_contents("admin/views/layout.html");

		$items = '<li><a href="#"><i class="fa '.$info['icon'].' fa-fw"></i> '.$info['name'].'<span class="fa arrow"></span></a><ul class="nav nav-second-level">'.implode("\n", $items)."</ul></li>";

		$lines = '{* PLUGIN <'.$this->slugify($info['name']).'> START *}'."\n";
		$lines .= $items."\n";
		$lines .= '{* PLUGIN <'.$this->slugify($info['name']).'> END *}'."\n\n";
		$lines .= '{* PLUGIN ROUTES *}'."\n\n";

		$admin_layout = str_replace('{* PLUGIN ROUTES *}', $lines, $admin_layout);
		file_put_contents("admin/views/layout.html", $admin_layout);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function getPluginInfo($plugin_dir_name)
	{
		$content = file_get_contents("admin/cache/upload/".$plugin_dir_name."/info.json");

		return json_decode($content, true);
	}

	public function underscore2Camelcase($str) {
		// Split string in words.
		$words = explode('_', strtolower($str));

		$return = [];
		foreach ($words as $word) {
		$return[] = ucfirst(trim($word));
		}

		return implode(" ", $return);
	}
}