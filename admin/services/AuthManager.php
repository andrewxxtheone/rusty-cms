<?php

class AuthManager {

	public $DI;

	public function __construct($di) {
		$this->DI = $di;
	}

	public function isAuthed() {

		if($this->DI->session->has("admin_id"))
			return true;

		return false;
	}

	public function login($email, $password) {
		$u = R::findOne('admin', 'email = ? and password = ?', [$email, $password]);
		if($u == null) {
			return false;
		}

		$this->DI->session->set('admin_id', $u['id']);
		return true;
	}
}