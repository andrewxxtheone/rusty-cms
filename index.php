<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();

$config = [
	
	'domain' => "http://rusty.bonzoportal.hu/",
	'db' => [
		'host' => 'localhost',
		'user' => 'c2rusty',
		'password' => 'j3JH5tJtZpXWeLDe',
		'database' => 'c2rusty'
	],
	'administrator' => [
		'email' => 'test@test.hu',
		'password' => 'test'
	],
	'facebook' => [
		'app_id' => "704041256394205",
		'app_secret' => 'e0ea77c7abdc9217bf442517eb55c099'
	]
];

if(strpos($_SERVER['REQUEST_URI'], "/admin") !== false) {
	require_once "admin/index.php";
	return;
}
require_once "app/index.php";