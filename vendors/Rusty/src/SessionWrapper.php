<?php

/**
 * Session interface
 *
 * @package Rusty
 * @author 
 **/
class SessionWrapper
{


	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function __construct()
	{
		@session_start();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function get($key, $default = null)
	{
		return ( $this->has($key)?$_SESSION['key']:$default );
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function set($key, $value)
	{
		$_SESSION[$key] = $value;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function has($key)
	{
		return array_key_exists($key, $_SESSION);
	}
} // END class SessionWrapper