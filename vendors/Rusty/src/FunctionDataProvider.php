<?php
use Dwoo\Compiler;
use Dwoo\ICompilable;
use Dwoo\Plugin;
class FunctionDataProvider extends Plugin implements ICompilable {

    public static function compile($value) {

    	return '$GLOBALS[\'di\']->dataProvider->dwoo('.$value.')';
    }
}