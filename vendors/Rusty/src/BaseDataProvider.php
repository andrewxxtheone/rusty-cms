<?php

/**
 * undocumented class
 *
 * @package default
 * @author 
 **/
class BaseDataProvider implements IDataProvider
{

	protected $di;

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function __construct($di)
	{
		$this->di = $di;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function __get($name)
	{
		return $this->di->{$name};
	}
} // END class BaseDataProvider implements IDataProvider