<?php

/**
 * Base Controller class
 *
 * @package Rusty
 * @author 
 **/
class ControllerBase
{

	/**
	 * undocumented class variable
	 *
	 * @var object
	 **/
	protected $klein;

	/**
	 * Setting Klien's service provider pointer
	 *
	 * @return void
	 * @author 
	 **/
	public function setKlein($klein)
	{
		$this->klein = $klein;
	}

	/**
	 * get DI's service
	 *
	 * @return Object
	 * @author 
	 **/
	public function __get($serviceName)
	{
		return $this->klein->service()->DI->{$serviceName};
	}



	public function slugify($text) { 
	  // replace non letter or digits by -
	  $text = str_replace(["ö", "ő", "ű", "ü"], ["o", "o", "u", "u"], $text);
	  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

	  // trim
	  $text = trim($text, '-');

	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  // lowercase
	  $text = strtolower($text);

	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  if (empty($text))
	  {
	    return 'n-a';
	  }

	  return $text;
	}
} // END class ControllerBase