<?php

/**
 * Data Provider Service
 *
 * @package default
 * @author 
 **/
class DataProvider
{

	/**
	 * undocumented class variable
	 *
	 * @var array
	 **/
	public $services;

	/**
	 * undocumented class variable
	 *
	 * @var DI
	 **/
	public $DI;

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function __construct($di)
	{
		$this->DI = $di;
		$this->services = [];
	}

	/**
	 * undocumented function
	 *
	 * @return array
	 * @author 
	 **/
	public function fetch($service, $method, $args)
	{

		if(!array_key_exists($service, $this->services)) {
            $refClass = new \ReflectionClass($service);
            $instance = $refClass->newInstance($this->DI);
            
            if(!($instance instanceof IDataProvider)) {
            	throw new Exception($service." is not instance of IDataProvider");
            }

            $this->services[$service] = $instance;
		}

		$this->parameterSanitizer($args);
		
		return call_user_func_array([$this->services[$service], $method], $args);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function dwoo($json)
	{
		$d = json_decode($json, 1);

		return $this->fetch($d['service'], $d['method'], $d['args']);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function parameterSanitizer(&$args)
	{
		foreach($args as $arg => &$val) {
			if(is_array($val)) {
				$this->parameterSanitizer($val);
				return;
			}

			if(strpos($val, "$") !== FALSE) {
				$param = substr($val, 1);
				$val = $this->DI->request->param($param);
			}
		}
	}

} // END class DataProvider