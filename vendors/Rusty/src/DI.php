<?php

/**
 * Lazy dependencí injection
 *
 * @package Rusty
 * @author 
 **/
class DI
{
	/**
	 * undocumented class variable
	 *
	 * @var array(key, callable)
	 **/
	protected $lazyTriggers;

	/**
	 * Setup services
	 *
	 * @var array(key, service)
	 **/
	protected $services;

	public function __construct() {
		$this->lazyTriggers = array();
		$this->services = array();
	}

	/**
	 * set Lazy loader
	 *
	 * @return void
	 * @author 
	 **/
	public function set($name, $lazyLoader)
	{
		$this->lazyTriggers[$name] = $lazyLoader;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function __get($name)
	{
		if(array_key_exists($name, $this->services)) {
			return $this->services[$name];
		}

		if(array_key_exists($name, $this->lazyTriggers)) {
			$this->services[$name] = $this->lazyTriggers[$name]();
			return $this->__get($name);
		}

		die("Service not found: ".$name);
	}
} // END class DI