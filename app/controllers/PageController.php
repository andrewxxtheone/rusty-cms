<?php

class PageController extends ControllerBase {

	public function viewAction() {

		/*$this->dataProvider->fetch("TestData", "test", [
			'baba' => 'baba',
			'arg' => '$limit'
		]);*/

		$page = R::findOne("page", "slug = ?", [$this->request->param('page_slug')]);
		$data = [];

		if(strlen($page->data_provider) > 0) {
			$arr = json_decode($page->data_provider, 1);

			$data = $this->dataProvider->fetch($arr['service'], $arr['method'], $arr['args']);
		}

		$this->view->output("pages/".$this->request->param('page_slug').".html", ['data' => $data]);
	}
}