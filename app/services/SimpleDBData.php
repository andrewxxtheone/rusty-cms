<?php

/**
 * undocumented class
 *
 * @package default
 * @author 
 **/
class SimpleDBData extends BaseDataProvider
{

	public function fetch($type, $where, $args) {
		//return [['id' => 0, 'title' => 'asd']];
		return ['rows' => R::find($type, $where, $args)];
	}
} // END class TestData