<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><a class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
    <span class="glyphicon glyphicon-chevron-down"></span>
</a>
<div class="nav-collapse collase">
    <ul class="nav navbar-nav">
        <?php $this->scope["data"]=$GLOBALS['di']->dataProvider->dwoo('{"service": "SimpleDBData", "method": "fetch", "args": ["page", "id > ?", [0]] }')?>
        <?php 
$_fh0_data = (isset($this->scope["data"]["rows"]) ? $this->scope["data"]["rows"]:null);
if ($this->isTraversable($_fh0_data) == true)
{
	foreach ($_fh0_data as $this->scope['val'])
	{
/* -- foreach start output */
?>
           <?php if ((isset($this->scope["val"]["type"]) ? $this->scope["val"]["type"]:null) == "page") {
?>
              <li><a href="/<?php echo $this->scope["val"]["slug"];?>"><?php echo $this->scope["val"]["title"];?></a></li>
           <?php 
}?>
        <?php 
/* -- foreach end output */
	}
}?>
    </ul>
    <ul class="nav navbar-right navbar-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-search"></i></a>
            <ul class="dropdown-menu" style="padding:12px;">
                <form class="form-inline">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                </form>
            </ul>
        </li>
    </ul>
</div><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>