<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ;
// checking for modification in file:app/views//layouts/simple-layout.html
if (!("1436826059" == filemtime('app/views//layouts/simple-layout.html'))) { ob_end_clean(); return false; };
// checking for modification in file:app/views//layouts/base.html
if (!("1436826766" == filemtime('app/views//layouts/base.html'))) { ob_end_clean(); return false; };?><!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Rusty CMS</title>
    <meta name="generator" content="Rusty" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Rusty CMS" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">

    <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <link rel="apple-touch-icon" href="/bootstrap/img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/bootstrap/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/bootstrap/img/apple-touch-icon-114x114.png">

    <style type="text/css">
        /* bootstrap 3 helpers */
        
        .navbar-form input,
        .form-inline input {
            width: auto;
        }
        /* end */
        /* custom theme */
        
        header {
            min-height: 230px;
            margin-bottom: 5px;
        }
        
        @media (min-width: 979px) {
            #sidebar.affix-top {
                position: static;
            }
            #sidebar.affix {
                position: fixed;
                top: 0;
                width: 21.2%;
            }
        }
        
        .affix,
        .affix-top {
            position: static;
        }
        /* theme */
        
        body {
            color: #828282;
            background-color: #eee;
        }
        
        a,
        a:hover {
            color: #ff3333;
            text-decoration: none;
        }
        
        .highlight-bk {
            background-color: #ff3333;
            padding: 1px;
            width: 100%;
        }
        
        .highlight {
            color: #ff3333;
        }
        
        h3.highlight {
            padding-top: 13px;
            padding-bottom: 14px;
            border-bottom: 2px solid #ff3333;
        }
        
        .navbar {
            background-color: #ff3333;
            color: #ffffff;
            border: 0;
            border-radius: 0;
        }
        
        .navbar-nav > li > a {
            color: #fff;
            padding-left: 20px;
            padding-right: 20px;
            border-left: 1px solid #ee3333;
        }
        
        .navbar-nav > li > a:hover,
        .navbar-nav > li > a:focus {
            color: #666666;
        }
        
        .navbar-nav > li:last-child > a {
            border-right: 1px solid #ee3333;
        }
        
        .navbar-nav > .active > a,
        .navbar-nav > .active > a:hover,
        .navbar-nav > .active > a:focus {
            color: #ffffff;
            background-color: transparent;
        }
        
        .navbar-nav > .open > a,
        .navbar-nav > .open > a:hover,
        .navbar-nav > .open > a:focus {
            color: #f0f0f0;
            background-color: transparent;
            opacity: .9;
            border-color: #ff3333;
        }
        
        .nav .open > a {
            border-color: #777777;
            border-width: 0;
        }
        
        .accordion-group {
            border-width: 0;
        }
        
        .dropdown-menu {
            min-width: 250px;
        }
        
        .accordion-heading .accordion-toggle,
        .accordion-inner,
        .nav-stacked li > a {
            padding-left: 1px;
        }
        
        .caret {
            color: #fff;
        }
        
        .navbar-toggle {
            color: #fff;
            border-width: 0;
        }
        
        .navbar-toggle:hover {
            background-color: #fff;
        }
        
        .panel {
            padding-left: 27px;
            padding-right: 27px;
        }
        /* end theme */
    </style>
</head>

<!-- HTML code from Bootply.com editor -->

<body>

    <nav class="navbar navbar-static">
        <div class="container">
              <?php echo $this->classCall('include', array("/partials/felso-menu.html", null, null, null, '_root', null));?>
        </div>
    </nav>
    <!-- /.navbar -->

    <header class="masthead">
        <div class="container">
            <div class="row">
                <div class="col col-sm-6">
                    <h1><a href="#" title="scroll down for your viewing pleasure">BNW Template</a>
          <p class="lead">2-column Layout + Theme for Bootstrap 3</p></h1>
                </div>
                <div class="col col-sm-6">
                    <div class="well pull-right">
                        <img src="//placehold.it/280x100/E7E7E7">
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col col-sm-12">

                    <div class="panel">
                        <div class="panel-body">
                            You may want to put some news here <span class="glyphicon glyphicon-heart-empty"></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </header>

    <!-- Begin Body -->
    <div class="container">

                <div class="row">
            <div class="col col-sm-3">
                <div id="sidebar">
                    <ul class="nav nav-stacked">
                        <li>
                            <h3 class="highlight">Channels <i class="glyphicon glyphicon-dashboard pull-right"></i></h3></li>
                        <li><a href="#">Link</a></li>
                        <li><a href="#">Link</a></li>
                    </ul>
                    <div class="accordion" id="accordion2">
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                Accordion
                            </a>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse in">
                                <div class="accordion-inner">
                                    <p>There is a lot to be said about RWD.</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                    Accordion
                                </a>
                            </div>
                            <div id="collapseTwo" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>Use @media queries or utility classes to customize responsiveness.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-sm-9">
                Tartalom..... <a href="http://rusty.bonzoportal.hu/masik-lap" title="tovabb">tovabb</a>
            </div>
        </div>
    </div>

    <script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script type='text/javascript' src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <!-- JavaScript jQuery code from Bootply.com editor  -->

    <script type='text/javascript'>
        $(document).ready(function() {

            $('#sidebar').affix({
                offset: {
                    top: 240
                }
            });

        });
    </script>

</body>

</html><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>