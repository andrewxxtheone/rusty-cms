<?php

spl_autoload_register(function ($class_name) 
{
	$class_name = str_replace("\\", "/", $class_name);
    //class directories
    $directorys = array(
        'app/',
        'app/controllers/',
        'app/services/',
        'vendors/Rusty/src/',
        'vendors/Klein/src/',
        'vendors/Facebook/src'
    );
    
    //for each directory
    foreach($directorys as $directory)
    {	

        //see if the file exsists
        if(file_exists($directory.$class_name . '.php'))
        {
            require_once($directory.$class_name . '.php');
            //only require the class once, so quit after to save effort (if you got more, then name them something else 
            return;
        }            
    }
});

require_once "vendors/Redbean/src/rb.php";
R::setup( 'mysql:host='.$config['db']['host'].';dbname='.$config['db']['database'], $config['db']['user'], $config['db']['password'] );

require_once "vendors/Dwoo/src/Dwoo/Autoloader.php";
// Register Dwoo namespace and register autoloader
$autoloader = new Dwoo\Autoloader();
$autoloader->add('Dwoo', 'vendors/Dwoo/src/Dwoo');
$autoloader->register(true);

require_once "vendors/Facebook/src/Facebook/autoload.php";