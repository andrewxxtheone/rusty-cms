<?php

require_once "AutoLoader.php";

$di = new DI();

$klein = new \Klein\Klein();

$di->set("request", function() use(&$klein) {
	return $klein->request();
});

$di->set("response", function() use(&$klein) {
	return $klein->response();
});

$di->set("dataProvider", function() use(&$di) {
	return new DataProvider($di);
});

$di->set("facebook", function() use(&$config) {
	$fb = new \Facebook\Facebook([
	  'app_id' => $config['facebook']['app_id'],
	  'app_secret' => $config['facebook']['app_secret'],
	  'default_graph_version' => 'v2.4',
	  ]);
	return $fb;
});

$di->set("view", function() {
	$core = new \Dwoo\Core();
	$core->setCompileDir('app/cache/views/'); 
	$core->setTemplateDir('app/views/');

	$core->addPlugin("dataProvider", "FunctionDataProvider", 1);
	return $core;
});

$di->set("session", function() {
	$sess = new SessionWrapper();
	return $sess;
});

$klein->respond(function ($request, $response, $service, $app) use($di) {
	$service->DI = $di;
});

$klein->respond('GET', '/', ["IndexController", "indexAction"]);





/*PLUGIN ROUTES*/





$klein->respond('/[a:controller]/[a:action]/[:param1]?/[:param2]?/[:param3]?/[:param4]?', function($request, $response, $service, $app) use(&$klein) {
    $refClass = new \ReflectionClass(ucwords($request->param('controller'))."Controller");
    $instance = $refClass->newInstance();

    $instance->setKlein($klein);

    call_user_func_array([$instance, $request->param('action')."Action"], [
    	$request->param('param1'),
    	$request->param('param2'),
    	$request->param('param3'),
    	$request->param('param4')
    ]);
});

$klein->respond('GET', '/[*:page_slug]', ["PageController", "viewAction"]);


$klein->dispatch();